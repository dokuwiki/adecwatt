<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <webmestre@parlenet.org>
 *
 * Options for the adecwatt Plugin
 */
$conf['dataDir']        = 'adecwatt';
$conf['adecWattGroup'] = 'adecwatt';
?>
