<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <webmestre@parlenet.org>
 *
 * Metadata for configuration manager plugin
 * Additions for the AdecWatt plugin
 */
$meta['dataDir']        = array('string');
$meta['adecWattGroup'] = array('string');
?>
