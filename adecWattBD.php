<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <webmestre@parlenet.org>
 *
 * Plugin Schedule: manage events per wiki @groups
 */

// faire un XML

if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../../').'/');
require_once (DOKU_INC.'inc/init.php');
require_once (DOKU_INC.'inc/common.php');
require_once (DOKU_INC.'inc/auth.php');
require_once (realpath (dirname (__FILE__)).'/adecWattBD.class.php');
{
    //phpinfo ();
    if (isset ($_SERVER['REMOTE_USER']))
        $INFO['userinfo'] = $auth->getUserData ($auth->cleanUser ($_SERVER['REMOTE_USER']));
    $adecplug =& plugin_load ('syntax', 'adecwatt');
    /* $isAdmin = false; */
    /* isset ($INFO ['userinfo']) && */
    /* isset ($INFO ['userinfo']['grps']) && */
    /* in_array (trim ($adecplug->getConf ('adminGroup')), $INFO ['userinfo']['grps']); */

    $adecWattDB = new adecWattDB ($adecplug);
    $version = '';
    if ($_REQUEST ['version'])
        $version = $_REQUEST ['version']."/"; // old invocation
    $path=$_REQUEST ['name'];

    if ('getRoles' == $_REQUEST ['action'])
        $adecWattDB->getRoles ($path);
    elseif ('zipList' == $_REQUEST ['action'])
        $adecWattDB->zipList ($version, $path);
    elseif ('zipGets' == $_REQUEST ['action'])
        $adecWattDB->zipGets ($version, $path);
    elseif ('zipPuts' == $_REQUEST ['action'])
        $adecWattDB->zipPuts ($version, $path);
    elseif ('zipRemove' == $_REQUEST ['action'])
        $adecWattDB->zipRemove ($version, $path);

    // not used
    elseif ('getZip' == $_REQUEST ['action'])
        $adecWattDB->getZip ($version, $path);

    // old
    elseif ('list' == $_REQUEST ['action'])
        $adecWattDB->getDir ($version, $path);
    elseif ('get' == $_REQUEST ['action'])
        $adecWattDB->getDataFile ($version, $path);
    else
        die ("No Ajax Function !");
}
