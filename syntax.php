<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois Merciol <webmestre@parlenet.org>
   *
   * Plugin AdecWatt: manage forms for adecWatt DB
   */
 
if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

// ============================================================
class syntax_plugin_adecwatt extends DokuWiki_Syntax_Plugin {
 
  // ============================================================
  function getType () { return 'substition'; }
  function getPType () { return 'block'; }
  function getSort () { return 299; }
  function connectTo ($mode) {
    $this->Lexer->addSpecialPattern ('\{\{adecwat56[^}]*\}\}', $mode, 'plugin_adecwatt');
  }
 
  // ============================================================
  function handle ($match, $state, $pos, Doku_Handler $handler) {
    switch ($state) {
    case DOKU_LEXER_SPECIAL :
      return trim (substr ($match, 12, -2)); // "{{adecwatt" => 12 "}}" => 2
    }
    return false;
  }

  // ============================================================
  function render ($mode, Doku_Renderer $renderer, $indata) {
    if ($mode != 'xhtml')
      return false;
    $data = " ".$indata." ";
    return true;
  }


  // ============================================================
} // syntax_plugin_LODGING
?>
