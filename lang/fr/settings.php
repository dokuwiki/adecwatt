<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <webmestre@parlenet.org>
 *
 * French language file
 */

// for the configuration manager
$lang['dataDir']        = 'répertoire où sont placer les données';
$lang['adecWattGroup'] = 'groupe de validation des propositions';
?>
